1;
global sin_cos_coeffs = [1, 1/2, -1/6, -1/24, 1/120, 1/720];

function [s,c]=thetaTerms(x)
  global sin_cos_coeffs;
  a=1;
  s=0;
  c=0;
  for (i=1: size(sin_cos_coeffs,2))
    if (mod(i-1,2)==0)
      s+= a * sin_cos_coeffs(i);
    else
      c+= a * sin_cos_coeffs(i);
    endif;
    a*=x;
  endfor;
endfunction

function [s,c]=dThetaTerms(x)
  global sin_cos_coeffs;
  a=1;
  s=0;
  c=0;
  for (i=2: size(sin_cos_coeffs,2))
    if (mod(i-1,2)==0)
      s+= (i-1)*a * sin_cos_coeffs(i);
    else
      c+= (i-1)*a * sin_cos_coeffs(i);
    endif;
    a*=x;
  endfor;
endfunction

function delta=predict(x,u)
  tl=u(1);
  tr=u(2);
  kl=x(1);
  kr=x(2);
  b =x(3);
  d_plus  = tr*kr+tl*kl;
  d_minus = tr*kr-tl*kl;
  dth     = d_minus/b;
  [s,c]   = thetaTerms(dth);
  dx      = 0.5 * d_plus * s;
  dy      = 0.5 * d_plus * c;
  delta = [dx, dy, dth]';
endfunction;

function J=Jpredict(x,u)
  J=zeros(3,3);
  dx=zeros(3,1);
  global epsilon=1e-4;
  for (i=1:size(x,1))
    dx(i)=epsilon;
    J(:,i)=predict(x+dx,u) - predict(x-dx,u);
    dx(i)=0;
  endfor;
  J*=.5/epsilon;
endfunction;

function [e,J]=errorAndJacobian(x,u,z)
  e=predict(x,u)-z;
  J=Jpredict(x,u);
endfunction;

function [UZ]=generateData(X,num_samples)
  UZ=zeros(5,num_samples);
  UZ(1:2,:)=rand(2, num_samples);
  for (i=1:num_samples)
    UZ(3:5,i)=predict(X,UZ(1:2,i));
  endfor;
endfunction

function [x_new,chi]=oneRound(x,UZ)
  H=zeros(3,3);
  b=zeros(3,1);
  chi=0;
  for (i=1:size(UZ,2))
    u=UZ(1:2,i);
    z=UZ(3:5,i);
    [e,J]=errorAndJacobian(x,u,z);
    H+=J'*J;
    b+=J'*e;
    chi+=e'*e;
  endfor;
  dx=-H\b;
  rank(H);
  x_new=x+dx;
endfunction

function [x_final, chi_stats]=solve(x_start,UZ,num_rounds)
  chi_stats=zeros(1, num_rounds);
  x_final=x_start;
  for (i=1:num_rounds)
    [x_final,chi_stats(i)]=oneRound(x_final,UZ);
  endfor;
endfunction;

